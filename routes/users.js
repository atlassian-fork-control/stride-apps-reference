const express = require('express');
const router = express.Router();
const formatHelper = require('../helpers/format');
const stride = require('../client');
const logger = require('../middleware/logger').logger;

/**
 * @name Users: get user details
 * @description
 * Get a user's details like name, email, id, avatar url.
 */

router.get('/userDetails', async (req, res, next) => {
  const loggerInfoName = 'user_details';

  try {
    const { cloudId, userId, conversationId } = res.locals.context;
    logger.info(`${loggerInfoName} incoming request for ${userId}`);

    let userDetailsResponse = await stride.api.users.get(cloudId, userId, {});

    let displayName = userDetailsResponse.displayName;
    let user_id = userDetailsResponse.id;
    let email = userDetailsResponse.emails[0]['value'];

    let message = formatHelper.messageWithUserInfo(displayName, email, user_id);

    await stride.api.messages.sendMessage(cloudId, conversationId, { body: message });

    res.sendStatus(200);
  } catch (err) {
    logger.error(`${loggerInfoName} error: ${err}`);
    res.sendStatus(500);
    next(err);
  }
});

/**
 * @name Users: mention a user in a message
 * @description
 * To mention a user, add a `mention` node to the message. You just need to specify the user's ID and Stride
 * takes care of the rest
 */
router.get('/userMention', async (req, res, next) => {
  const loggerInfoName = 'user_mention';

  try {
    const { cloudId, userId, conversationId } = res.locals.context;
    logger.info(`${loggerInfoName} incoming request for ${userId}`);

    let user = await stride.api.users.get(cloudId, userId, {});
    let userIdToMention = user.id;
    let message = formatHelper.messageWithUserMention(userIdToMention);
    await stride.api.messages.sendMessage(cloudId, conversationId, { body: message });

    res.sendStatus(200);
  } catch (err) {
    logger.error(`${loggerInfoName} error: ${err}`);
    res.sendStatus(500);
    next(err);
  }
});

module.exports = router;
