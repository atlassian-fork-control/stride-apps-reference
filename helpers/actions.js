const { Document } = require('adf-builder');

const app_name = process.env.APP_NAME || 'Stride Reference App';

module.exports.messageWithCard = () => {
  let doc = new Document();

  let card = doc
    .applicationCard('Action Cards')
    .description('See the triple dot ( ellipsis) on this card for more actions.');

  //Call a service points to a target URL /action/reference-service
  card
    .action()
    .title('Call service')
    .target({ key: 'actionTarget-handleCardAction' });

  card
    .action()
    .title('Open dialog')
    .target({ key: 'actionTarget-sendToDialog' })
    .parameters({ 'custom-param-from-card': 'some value' });

  //Call a service, then open a dialog
  card
    .action()
    .title('Call service then open dialog')
    .target({ key: 'actionTarget-handleCardAction' })
    .parameters({ then: 'openDialog' });

  card.context(`${app_name}: Action cards example`).icon({
    url: 'https://image.ibb.co/fPPAB5/Stride_White_On_Blue.png',
    label: 'Stride',
  });

  return doc.toJSON();
};

module.exports.messageWithCardThatUpdates = () => {
  let doc = new Document();

  let card = doc
    .applicationCard('Incident #4253')
    .link('https://www.atlassian.com')
    .description('Something is broken');
  card
    .action()
    .title('Ack')
    .target({ key: 'actionTarget-updateCard' })
    .parameters({ incidentAction: 'ack' });
  card
    .action()
    .title('Resolve')
    .target({ key: 'actionTarget-updateCard' })
    .parameters({ incidentAction: 'resolve' });
  card.context('DevOps / Incidents').icon({
    url: 'https://image.ibb.co/fPPAB5/Stride_White_On_Blue.png',
    label: 'Stride',
  });

  return doc.toJSON();
};

module.exports.ackMessage = messageToAck => {
  messageToAck.content.unshift({
    type: 'paragraph',
    content: [{ type: 'text', text: 'You just triggered an action for:' }],
  });
};

module.exports.messageWithActionMark = () => {
  return {
    version: 1,
    type: 'doc',
    content: [
      {
        type: 'paragraph',
        content: [
          {
            type: 'text',
            text: "Click to open the app's dialog",
            marks: [
              {
                type: 'action',
                attrs: {
                  title: 'open dialog',
                  target: {
                    key: 'actionTarget-sendToDialog',
                  },
                  parameters: {
                    'custom-param-from-actionmark': 'Some value',
                  },
                },
              },
            ],
          },
        ],
      },
    ],
  };
};

module.exports.messageWithInlineActionGroup = () => {
  return {
    version: 1,
    type: 'doc',
    content: [
      {
        type: 'paragraph',
        content: [
          {
            type: 'text',
            text: 'Do you approve?',
          },
        ],
      },
      {
        type: 'paragraph',
        content: [
          {
            type: 'inlineExtension',
            attrs: {
              extensionType: 'com.atlassian.stride',
              extensionKey: 'actionGroup',
              parameters: {
                key: 'inline-action-group-key',
                actions: [
                  {
                    key: 'approve',
                    title: 'Approve',
                    appearance: 'primary',
                    action: {
                      target: {
                        key: 'actionTarget-handleInlineMessageAction',
                      },
                      parameters: {
                        uniqueId: 1,
                      },
                    },
                  },
                  {
                    key: 'reject',
                    title: 'Reject',
                    appearance: 'default',
                    action: {
                      target: {
                        key: 'actionTarget-handleInlineMessageAction',
                      },
                      parameters: {
                        uniqueId: 2,
                      },
                    },
                  },
                ],
              },
            },
          },
        ],
      },
    ],
  };
};

module.exports.messageWithInlineActionGroupResponse = (userId, reason) => {
  let approvalTxt = reason === 'approve' ? 'Approved by ' : 'Rejected by ';

  const doc = new Document();
  doc.paragraph().text('Do you approve?');
  doc
    .paragraph()
    .strong(approvalTxt)
    .mention(userId);
  return doc.toJSON();
};

module.exports.messageWithInlineSelect = () => {
  const document = {
    version: 1,
    type: 'doc',
    content: [
      {
        type: 'paragraph',
        content: [
          {
            type: 'text',
            text: 'What should I do next?',
            marks: [
              {
                type: 'strong',
              },
            ],
          },
        ],
      },
      {
        type: 'paragraph',
        content: [
          {
            type: 'text',
            text: 'Call a service exposed by the app, then execute another action client-side:',
          },
        ],
      },
      {
        type: 'paragraph',
        content: [
          {
            type: 'inlineExtension',
            attrs: {
              extensionType: 'com.atlassian.stride',
              extensionKey: 'select',
              parameters: {
                key: 'inlineSelect-nextAction',
                title: 'Choose next action',
                source: 'custom',
                data: {
                  value: '',
                  options: [
                    {
                      value: 'openSidebar',
                      title: 'Open a sidebar',
                    },
                    {
                      value: 'openDialog',
                      title: 'Open a dialog',
                    },
                    {
                      value: 'openHighlights',
                      title: 'Open room highlights',
                    },
                    {
                      value: 'openRoom',
                      title: 'Create and open a room',
                    },
                    {
                      value: 'openFiles',
                      title: 'Open room files and links',
                    },
                  ],
                },
                action: {
                  target: {
                    key: 'actionTarget-handleInlineMessageSelect',
                  },
                },
              },
            },
          },
        ],
      },
      {
        type: 'paragraph',
        content: [
          {
            type: 'text',
            text: 'Open a dialog, passing the following custom parameter:',
          },
        ],
      },
      {
        type: 'paragraph',
        content: [
          {
            type: 'inlineExtension',
            attrs: {
              extensionType: 'com.atlassian.stride',
              extensionKey: 'select',
              parameters: {
                key: 'inlineSelect-sendToDialog',
                title: 'Choose parameter value',
                source: 'custom',
                data: {
                  value: '',
                  options: [
                    {
                      value: 'value1',
                      title: 'Value 1',
                    },
                    {
                      value: 'value2',
                      title: 'Value 2',
                    },
                  ],
                },
                action: {
                  target: {
                    key: 'actionTarget-sendToDialog',
                  },
                },
              },
            },
          },
        ],
      },
      {
        type: 'paragraph',
        content: [
          {
            type: 'text',
            text: 'Open a dialog, passing the selected user:',
          },
        ],
      },
      {
        type: 'paragraph',
        content: [
          {
            type: 'inlineExtension',
            attrs: {
              extensionType: 'com.atlassian.stride',
              extensionKey: 'select',
              parameters: {
                key: 'inlineSelect-sendUserToDialog',
                title: 'Start typing a user name',
                source: 'users_in_site',
                action: {
                  target: {
                    key: 'actionTarget-sendToDialog',
                  },
                },
              },
            },
          },
        ],
      },
      {
        type: 'paragraph',
        content: [
          {
            type: 'text',
            text: 'Open a dialog, passing the selected room:',
          },
        ],
      },
      {
        type: 'paragraph',
        content: [
          {
            type: 'inlineExtension',
            attrs: {
              extensionType: 'com.atlassian.stride',
              extensionKey: 'select',
              parameters: {
                key: 'inlineSelect-sendRoomToDialog',
                title: 'Start typing a room name',
                source: 'rooms_in_site',
                action: {
                  target: {
                    key: 'actionTarget-sendToDialog',
                  },
                },
              },
            },
          },
        ],
      },
    ],
  };

  return document;
};

module.exports.messageWithInlineSelectResponse = () => {
  const doc = new Document();
  doc.paragraph().text('Done. You can try again.');
  return doc.toJSON();
};

module.exports.messageWithExpandNode = (userId, conditions, reason) => {
  const parameters = {
    key: 'expandable-content',
  };

  if (conditions) {
    parameters.conditions = conditions;
  }

  let header = {
    type: 'paragraph',
    content: [
      {
        type: 'mention',
        attrs: {
          id: userId,
        },
      },
      {
        type: 'text',
        text: ' you have a new expense report to approve',
      },
    ],
  };

  if (reason) {
    header = {
      type: 'paragraph',
      content: [
        {
          type: 'text',
          text: `An expense report was ${reason === 'approve' ? 'approved' : 'rejected'} by `,
        },
        {
          type: 'mention',
          attrs: {
            id: userId,
          },
        },
      ],
    };
  }

  const buttons = {
    type: 'paragraph',
    content: [
      {
        type: 'inlineExtension',
        attrs: {
          extensionType: 'com.atlassian.stride',
          extensionKey: 'actionGroup',
          parameters: {
            key: 'inline-action-group-key',
            actions: [
              {
                key: 'approve',
                title: 'Approve',
                appearance: 'primary',
                action: {
                  target: {
                    key: 'actionTarget-handleExpandAction',
                  },
                  parameters: {
                    reason: 'approve',
                  },
                },
              },
              {
                key: 'reject',
                title: 'Reject',
                appearance: 'default',
                action: {
                  target: {
                    key: 'actionTarget-handleExpandAction',
                  },
                  parameters: {
                    reason: 'reject',
                  },
                },
              },
            ],
          },
        },
      },
    ],
  };

  const doc = {
    version: 1,
    type: 'doc',
    content: [
      {
        type: 'bodiedExtension',
        attrs: {
          extensionType: 'com.atlassian.stride',
          extensionKey: 'expand',
          parameters,
        },
        content: [
          header,
          {
            type: 'bulletList',
            content: [
              {
                type: 'listItem',
                content: [
                  {
                    type: 'paragraph',
                    content: [
                      {
                        type: 'text',
                        text: 'Staff: Bob Smith',
                      },
                    ],
                  },
                ],
              },
              {
                type: 'listItem',
                content: [
                  {
                    type: 'paragraph',
                    content: [
                      {
                        type: 'text',
                        text: 'Amount: $300',
                      },
                    ],
                  },
                ],
              },
              {
                type: 'listItem',
                content: [
                  {
                    type: 'paragraph',
                    content: [
                      {
                        type: 'text',
                        text: 'Justification: office supplies',
                      },
                    ],
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  };

  if (!reason) {
    doc.content[0].content.push(buttons);
  }

  return doc;
};
